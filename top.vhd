library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;    --možnost použít znaménko +

--------------------------------------------------------------------------------
-- Entity declaration for top level
--------------------------------------------------------------------------------
entity top is
    port (
        -- Global input signals at CPLD expansion board
        cpld_sw_i : in std_logic_vector(12-1 downto 0);   
        clk_i : in std_logic;   
        cpld_led_o : out std_logic_vector(1-1 downto 0)
    );
end top;

--------------------------------------------------------------------------------
-- Architecture declaration for top level
--------------------------------------------------------------------------------
architecture Behavioral of top is

        type uart_t is (
            STATE_IDLE,
            STATE_START,
            STATE_DATA0,
            STATE_DATA1,
            STATE_DATA2,
            STATE_DATA3,
            STATE_DATA4,
            STATE_DATA5,
            STATE_DATA6,
            STATE_DATA7,
            STATE_PARITY,
            STATE_STOP,
            STATE_STOP2 
        );
        

        signal clk_tx: std_logic := '0';
        signal tmp: std_logic_vector(11 downto 0) := x"000";
        signal parity: std_logic:= '0';
        signal pom_par: std_logic_vector(7 downto 0);
        signal uart: uart_t:= STATE_IDLE;
        
        
begin
    
    process (clk_i)    --předdělička 100x
    begin
        if rising_edge(clk_i) then
            tmp <= tmp + 1;
            if tmp = x"032" then       --dec 50
                tmp <= x"000";
                clk_tx <= not clk_tx;
            end if;
        end if;
    end process;  


    process(clk_tx) 
    begin
        if rising_edge(clk_tx) then
            if cpld_sw_i(9)='0' then--reset
                uart <= STATE_IDLE;
            else            
                 case uart is
                    when STATE_IDLE  =>
                            if cpld_sw_i(9)='0' then
                            uart <= STATE_IDLE;
                            else
                            uart <=STATE_START;
                            end if;
                        cpld_led_o(0) <='1';
                    
                    when STATE_START => 
                        uart <=STATE_DATA0;
                        cpld_led_o(0) <='0';                        
                                                
                    when STATE_DATA0 => 
                        uart <= STATE_DATA1;
                        cpld_led_o(0) <= cpld_sw_i(0);
                        pom_par(0) <= cpld_sw_i(0); 
                        
                    when STATE_DATA1 =>     
                        uart <=STATE_DATA2;
                        cpld_led_o(0) <=cpld_sw_i(1); 
                        pom_par(1) <=cpld_sw_i(1);
                        
                    when STATE_DATA2 =>  
                        uart <=STATE_DATA3;
                        cpld_led_o(0) <=cpld_sw_i(2); 
                        pom_par(2) <=cpld_sw_i(2);
                        
                    when STATE_DATA3 =>  
                        uart <=STATE_DATA4;
                        cpld_led_o(0) <=cpld_sw_i(3);  
                        pom_par(3) <=cpld_sw_i(3);
                        
                    when STATE_DATA4 => 
                        uart <=STATE_DATA5;
                        cpld_led_o(0) <=cpld_sw_i(4);  
                        pom_par(4) <=cpld_sw_i(4);
                        
                    when STATE_DATA5 => 
                        uart <=STATE_DATA6;
                        cpld_led_o(0) <=cpld_sw_i(5);  
                        pom_par(5) <=cpld_sw_i(5);
                        
                    when STATE_DATA6 => 
                        uart <=STATE_DATA7;
                        cpld_led_o(0) <=cpld_sw_i(6);  
                        pom_par(6) <=cpld_sw_i(6);
                        
                    when STATE_DATA7 => 
                        cpld_led_o(0) <=cpld_sw_i(7);
                        pom_par(7) <=cpld_sw_i(7);       
                        if cpld_sw_i(8) = '1' then
                             uart <=STATE_PARITY;     
                        else                             
                             uart <=STATE_STOP;           --parity none    
                        end if;                                            
                        
                        
                    when STATE_PARITY =>
                            parity <=pom_par(7) xor pom_par(6) xor pom_par(5)
                                         xor pom_par(4) xor pom_par(3) xor pom_par(2)
                                         xor pom_par(1) xor pom_par(0);
                                         
                            if cpld_sw_i(10) = '0' then        --parity odd (lichá)
                                cpld_led_o(0) <= parity ;      
                                
                            else                               --parity even (sudá)
                                cpld_led_o(0) <= not parity;                                
                            end if; 
                                                       
                            uart <=STATE_STOP;
                         

                    when STATE_STOP  =>

                         
                         if cpld_sw_i(11) = '0' then            --2 stop bity
                             cpld_led_o(0) <='1'; 
                             uart <=STATE_STOP2;   
                         else
                             cpld_led_o(0) <='1';               --1 stop bit
                             uart <=STATE_START;                
                         end if;  
                         
                     when STATE_STOP2 =>
                         cpld_led_o(0) <='1'; 
                         uart <=STATE_START; 
                         

                    end case;                         
                 end if;
             end if;                  
    end process;
    
end Behavioral;